<?php $currency_symbol = $this->customlib->getSchoolCurrencyFormat(); ?>
<!-- Content Wrapper. Contains page content -->
<div class="content-wrapper">

    

    <!-- Main content -->
    <section class="content">
        <div class="row">
            <?php
            if ($this->rbac->hasPrivilege('subject_group', 'can_add')) {
                ?>
                <div class="col-md-4">
                    <!-- Horizontal Form -->
                    <div class="box box-primary">
                        <div class="box-header with-border">
                           <h3 class="box-title"><?php echo $this->lang->line('add'); ?> <?php echo $this->lang->line('virtualclass'); ?> <?php echo $this->lang->line('room'); ?></h3>
                       </div><!-- /.box-header -->
                       <form id="form1" action="<?php echo site_url('admin/virtualmeeting/createvirtualclass') ?>"  id="employeeform" name="employeeform" method="post" accept-charset="utf-8">

                         <div class="box-body">
                            <?php if ($this->session->flashdata('msg')) { ?>
                                <?php echo $this->session->flashdata('msg') ?>
                            <?php } ?>
                            <?php
                            if (isset($error_message)) {
                                echo "<div class='alert alert-danger'>" . $error_message . "</div>";
                            }
                            ?>
                            <?php echo $this->customlib->getCSRF(); ?>

                            <div class="form-group">
                                <label for="exampleInputEmail1"><?php echo $this->lang->line('virtualclass'); ?> <?php echo $this->lang->line('room'); ?></label> <small class="req">*</small>
                                <input autofocus="" id="name" name="name" placeholder="" type="text" class="form-control"  value="<?php echo set_value('name'); ?>" />
                                <span class="text-danger"><?php echo form_error('name'); ?></span>
                            </div>
                            <div class="form-group">

                                <label for="exampleInputEmail1"><?php echo $this->lang->line('class'); ?> </label><small class="req"> *</small>

                                <select  id="class_id" name="class_id" class="form-control" >
                                    <option value=""><?php echo $this->lang->line('select'); ?></option>
                                    <?php
                                    foreach ($classlist as $class) {
                                        ?>
                                        <option value="<?php echo $class['id'] ?>" <?php
                                        if (set_value('class_id') == $class['id']) {
                                            echo "selected=selected";
                                        }
                                        ?>>
                                        <?php echo $class['name'] ?></option>
                                        <?php
                                    }
                                    ?>
                                </select>
                                <span class="text-danger"><?php echo form_error('class_id'); ?></span>
                            </div>
                            
                        </div><!-- /.box-body -->

                        <div class="box-footer">
                            <button type="submit" class="btn btn-info pull-right"><?php echo $this->lang->line('save'); ?></button>
                        </div>
                    </form>
                </div>

            </div><!--/.col (right) -->
            <!-- left column -->
        <?php } ?>
        <div class="col-md-<?php
        if ($this->rbac->hasPrivilege('subject_group', 'can_add')) {
            echo "8";
            } else {
                echo "12";
            }
            ?>"> 
            <!-- general form elements -->
            <div class="box box-primary">
                <div class="box-header ptbnull">
                    <h3 class="box-title titlefix"><?php echo $this->lang->line('virtualclass'); ?> <?php echo $this->lang->line('room'); ?> <?php echo $this->lang->line('list'); ?></h3>
                    <div class="box-tools pull-right">
                    </div><!-- /.box-tools -->
                </div><!-- /.box-header -->
                <div class="box-body">
                    
                    <div class="table-responsive mailbox-messages" id="subject_list">
                        <div class="download_label"><?php echo $this->lang->line('subject')." ".$this->lang->line('group')." ".$this->lang->line('list'); ?></div>
                        
                        <a class="btn btn-default btn-xs pull-right" id="print" onclick="printDiv()" ><i class="fa fa-print"></i></a> <a class="btn btn-default btn-xs pull-right" id="btnExport" onclick="fnExcelReport();"> <i class="fa fa-file-excel-o"></i> </a>
                        <table class="table table-striped  table-hover " id="headerTable">
                            <thead>
                                <tr>
                                    <th><?php echo $this->lang->line('class') . "  " . $this->lang->line('name'); ?></th>
                                    <th><?php echo $this->lang->line('meeting') . "  " . $this->lang->line('name'); ?></th>
                                    <th><?php echo $this->lang->line('meetingid'); ?></th>
                                    <th style="text-align: center;"><?php echo $this->lang->line('action'); ?></th>
                                </tr>
                            </thead>
                            
                            <tbody>
                                <?php
                                foreach ($vClassList as $subjectgroup) {
                                    ?>
                                    <tr>
                                        <td class="mailbox-name">
                                            <a href="#" data-toggle="popover" class="detail_popover"><?php echo $subjectgroup->class." ".$subjectgroup->section; ?></a>
                                            

                                            <div class="fee_detail_popover" style="display: none">
                                                <?php
                                                if ($subjectgroup->description == "") {
                                                    ?>
                                                    <p class="text text-danger"><?php echo $this->lang->line('no_description'); ?></p>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <p class="text text-info"><?php echo $subjectgroup->description; ?></p>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </td>
                                        <td class="mailbox-name">
                                            <a href="#" data-toggle="popover" class="detail_popover"><?php echo $subjectgroup->meetingName; ?></a>

                                            <div class="fee_detail_popover" style="display: none">
                                                <?php
                                                if ($subjectgroup->description == "") {
                                                    ?>
                                                    <p class="text text-danger"><?php echo $this->lang->line('no_description'); ?></p>
                                                    <?php
                                                } else {
                                                    ?>
                                                    <p class="text text-info"><?php echo $subjectgroup->description; ?></p>
                                                    <?php
                                                }
                                                ?>
                                            </div>
                                        </td>
                                        
                                    </td>
                                    <td class="mailbox-name">
                                        <a href="#" data-toggle="popover" class="detail_popover"><?php echo $subjectgroup->meetingID; ?></a>

                                        <div class="fee_detail_popover" style="display: none">
                                            <?php
                                            if ($subjectgroup->description == "") {
                                                ?>
                                                <p class="text text-danger"><?php echo $this->lang->line('no_description'); ?></p>
                                                <?php
                                            } else {
                                                ?>
                                                <p class="text text-info"><?php echo $subjectgroup->description; ?></p>
                                                <?php
                                            }
                                            ?>
                                        </div>
                                    </td>
                                    
                                    <td class="mailbox-date pull-right no_print">
                                      <?php
                                      if ($this->rbac->hasPrivilege('subject_group', 'can_delete')) {
                                        ?>
                                        <a  href="<?php echo base_url(); ?>admin/Virtualclass/delete/<?php echo $subjectgroup->meetingID; ?>"class="btn btn-default btn-xs no_print"  data-toggle="tooltip" title="<?php echo $this->lang->line('delete'); ?>" onclick="return confirm('<?php echo $this->lang->line('delete_confirm') ?>');">
                                            <i  class="fa fa-remove"></i>
                                        </a>
                                    <?php } ?>
                                </td>
                            </tr>
                            <?php
                        }
                        ?>
                    </tbody>
                    
                    
                    
                    
                    
                    
                    
                    
                </table><!-- /.table -->



            </div><!-- /.mail-box-messages -->
        </div><!-- /.box-body -->
    </div>
</div><!--/.col (left) -->

<!-- right column -->

</div>
<div class="row">
    <!-- left column -->

    <!-- right column -->
    <div class="col-md-12">

    </div><!--/.col (right) -->
</div>   <!-- /.row -->
</section><!-- /.content -->
</div><!-- /.content-wrapper -->


<script>
   $(".no_print").css("display","block");
document.getElementById("print").style.display = "block";
  document.getElementById("btnExport").style.display = "block";

        function printDiv() { 
         $(".no_print").css("display","none");
            document.getElementById("print").style.display = "none";
             document.getElementById("btnExport").style.display = "none";
            var divElements = document.getElementById('subject_list').innerHTML;
            var oldPage = document.body.innerHTML;
            document.body.innerHTML = 
              "<html><head><title></title></head><body>" + 
              divElements + "</body>";
            window.print();
            document.body.innerHTML = oldPage;

            location.reload(true);
        }
    
 function fnExcelReport()
{
    var tab_text="<table border='2px'><tr >";
    var textRange; var j=0;
    tab = document.getElementById('headerTable'); // id of table

    for(j = 0 ; j < tab.rows.length ; j++) 
    {     
        tab_text=tab_text+tab.rows[j].innerHTML+"</tr>";
        //tab_text=tab_text+"</tr>";
    }

    tab_text=tab_text+"</table>";
    tab_text= tab_text.replace(/<A[^>]*>|<\/A>/g, "");//remove if u want links in your table
    tab_text= tab_text.replace(/<img[^>]*>/gi,""); // remove if u want images in your table
    tab_text= tab_text.replace(/<input[^>]*>|<\/input>/gi, ""); // reomves input params

    var ua = window.navigator.userAgent;
    var msie = ua.indexOf("MSIE "); 

    if (msie > 0 || !!navigator.userAgent.match(/Trident.*rv\:11\./))      // If Internet Explorer
    {
        txtArea1.document.open("txt/html","replace");
        txtArea1.document.write(tab_text);
        txtArea1.document.close();
        txtArea1.focus(); 
        sa=txtArea1.document.execCommand("SaveAs",true,"Say Thanks to Sumit.xls");
    }  
    else                 //other browser not tested on IE 11
        sa = window.open('data:application/vnd.ms-excel,' + encodeURIComponent(tab_text));  

    return (sa);
}

</script>