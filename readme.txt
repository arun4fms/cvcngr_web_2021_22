﻿Following are details about folders present in your downloaded package -

documentation:						Complete User’s Manual for understanding how Smart School Zoom Live Class version 1.0 addon can be use.
licensing:							GPL license files.
samrt_school_zoom_live_class_src:	Smart School Zoom Live Class addon source code files.
smart_school_zoom_live_class_update_1.0_to_2.0:		Smart School Zoom Live Class addon update source code files to update your older Smart School Zoom Live Class version 1.0 to 2.0.



For help and support, feel free to contact us http://smart-school.in or open support ticket at our support portal http://support.qdocs.in or email us support@qdocs.in .

Thanks for using Smart School and Smart School Zoom Live Class addon.
QDOCS Support Team