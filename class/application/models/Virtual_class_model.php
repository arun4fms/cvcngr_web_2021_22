<?php

if (!defined('BASEPATH')) {
    exit('No direct script access allowed');
}

class Virtual_class_model extends MY_Model {

    public function __construct() {
        parent::__construct();
        $this->current_session = $this->setting_model->getCurrentSession();
    }

    /**
     * This funtion takes id as a parameter and will fetch the record.h
     * If id is not provided, then it will fetch all the records form the table.
     * @param int $id
     * @return mixed
     */

    public function addaa() {

    }
    
     public function getserver($class_id) {
        
        $this->db->select("authkey,url");
        $this->db->from('virtual_class_config');
        $this->db->where_in('is_active', 'enabled');
        $this->db->where_in('name', 'server1');
        $query = $this->db->get();
        $resultApi = $query->row();
        return $resultApi; 
         
    /*     $servertwo= array(1,2,13,14);
        if(in_array($class_id, $servertwo)){
         
          $this->db->select("authkey,url");
        $this->db->from('virtual_class_config');
        $this->db->where_in('is_active', 'enabled');
        $this->db->where_in('name', 'server2');
        $query = $this->db->get();
         // print_r($query->row()); die();
        $resultApi = $query->row();
       
        return $resultApi;
        }
        else{
          
          $this->db->select("authkey,url");
        $this->db->from('virtual_class_config');
        $this->db->where_in('is_active', 'enabled');
        $this->db->where_in('name', 'server1');
        $query = $this->db->get();
        $resultApi = $query->row();
        return $resultApi;
        }
      */ 
    }
    

    public function get()
    {
        $this->db->select("meetingName,classes.class as class, sections.section as section ,sectionId,meetingID");
        $this->db->from('virtual_class_rooms');
        $this->db->join('classes', 'classes.id = virtual_class_rooms.classId');
        $this->db->join('sections', 'sections.id = virtual_class_rooms.sectionId');
        $this->db->order_by("class", "asc");
        $query = $this->db->get();
        return $query->result();
    }

    public function getApi()
    {
        $this->db->select("authkey,url");
        $this->db->from('virtual_class_config');
        $this->db->where_in('is_active', 'enabled');
        $query = $this->db->get();
        $resultApi = $query->row();
        return $resultApi;
    }
    
    public function update($data) {

    }

    public function check_data_exists($data) {

    }
    
    
    
    
    
    
    public function getStudentUrl($class_id, $section_id, $login_name)
    {
       // print_r($apiResult->url); die($class_id);
        $apiResult        = $this->getserver($class_id);
        // print_r($apiResult->url); die($class_id);
        $sql = "SELECT `meetingID`, `attendeePW` FROM virtual_class_rooms WHERE `classId` = ".$class_id." AND `sectionId` =".$section_id;
        $query = $this->db->query($sql);
        $user = $query->row();
        $meeting_ID = $user->meetingID;
        $attendee_PW = $user->attendeePW;
        $user_name = preg_replace('/\s+/', '-', $login_name);
        
        $studentquery = 'fullName='.$user_name.'&meetingID='.$meeting_ID.'&password='.$attendee_PW.'&redirect=true';
        $apichecksum = sha1('join'.$studentquery.$apiResult->authkey);
        $loginurl = $apiResult->url.'/join?'.$studentquery.'&checksum='.$apichecksum;
        ($loginurl);
        
  // print_r($loginurl); die();

        return $array = array(
         'login_url' => $loginurl,
         'meetingid' => $meeting_ID,
         'studentid' => $user_name,
     );
    }
    
     public function getTeacherUrl($meeting_id, $class_id, $staffId)
    {
        //
        $apiResult        = $this->getserver($class_id);
        
         //$apiResult        = $this->getApi();
        
         $this->db->select('name');
        $this->db->from('staff');
        $this->db->where('id', $staffId);
        $query = $this->db->get()->result();
        
    
        
        $url = "fullName=".$query[0]->name."&meetingID=".$meeting_id."&password=mp&redirect=true";
        
        $apichecksum = sha1('join'.$url.$apiResult->authkey);
        $loginurl = $apiResult->url.'/join?'.$url.'&checksum='.$apichecksum;
        
      //  print_r($loginurl); die();
     
     
     return $loginurl;
    }
    
    
    public function edit($data, $delete_sections, $add_sections, $delete_subjects, $add_subjects) {


    }
    
    /*get staff time table*/
    public function getByStaffandDay($staff_id, $day_value)
    {

        $apiResult        = $this->getApi();
        //$sql   = "SELECT DISTINCT CONCAT ('".$apiResult->url."/join?"."',`fullname`,staff.name,`meetingIDname`,`meetingID`,`passwordtxt`,`moderatorPW`,`endattach`, `ckeksname`, SHA1(CONCAT (`joinn`,`fullname`,staff.name,`meetingIDname`,`meetingID`,`passwordtxt`,`moderatorPW`,`endattach`,'".$apiResult->authkey."'))) as 'url', virtual_class_rooms.meetingID as 'username', virtual_class_rooms.moderatorPW as 'usercode', classes.class,sections.section,subject_group_subjects.subject_id,sub.name as subject_name,sub.code as subject_code,virtual_class_timetable.* FROM virtual_class_timetable LEFT JOIN classes on classes.id = virtual_class_timetable.class_id INNER JOIN virtual_class_rooms on virtual_class_rooms.classId = virtual_class_timetable.class_id INNER JOIN sections on sections.id=virtual_class_timetable.section_id INNER JOIN subject_group_subjects on subject_group_subjects.id=virtual_class_timetable.subject_group_subject_id INNER JOIN subjects as sub on sub.id=subject_group_subjects.subject_id INNER JOIN staff on staff.id = virtual_class_timetable.staff_id  WHERE virtual_class_timetable.staff_id=" . $this->db->escape($staff_id) . " and virtual_class_timetable.day=" . $this->db->escape($day_value);
       $sql   = "SELECT CONCAT ('".$apiResult->url."/join?"."',`fullname`,staff.name,`meetingIDname`,`meetingID`,`passwordtxt`,`moderatorPW`,`endattach`, `ckeksname`, SHA1(CONCAT (`joinn`,`fullname`,staff.name,`meetingIDname`,`meetingID`,`passwordtxt`,`moderatorPW`,`endattach`,'".$apiResult->authkey."'))) as 'url', virtual_class_rooms.meetingID as 'username', virtual_class_rooms.moderatorPW as 'usercode', classes.class, classes.id as 'class_id',sections.section,subject_group_subjects.subject_id,sub.name as subject_name,sub.code as subject_code,virtual_class_timetable.* FROM virtual_class_timetable INNER JOIN classes on classes.id = virtual_class_timetable.class_id INNER JOIN virtual_class_rooms on virtual_class_rooms.classId = virtual_class_timetable.class_id AND virtual_class_timetable.section_id = virtual_class_rooms.sectionId INNER JOIN sections on sections.id=virtual_class_timetable.section_id INNER JOIN subject_group_subjects on subject_group_subjects.id=virtual_class_timetable.subject_group_subject_id INNER JOIN subjects as sub on sub.id=subject_group_subjects.subject_id INNER JOIN staff on staff.id = virtual_class_timetable.staff_id  WHERE virtual_class_timetable.staff_id=" . $this->db->escape($staff_id) . " and virtual_class_timetable.day=" . $this->db->escape($day_value);
        $query = $this->db->query($sql);
        if ($query->num_rows() > 0) {
            return $query->result();
        } else {
            return false;
        }
    }
    

    
    
    public function getparentSubjectByClassandSectionDay($class_id, $section_id, $day)
    {
       $sql = "SELECT `subject_group_subjects`.`subject_id`,subjects.name as `subject_name`,subjects.code,subjects.type,staff.name,staff.surname,staff.employee_id,`virtual_class_timetable`.* FROM `virtual_class_timetable` JOIN `subject_group_subjects` ON `virtual_class_timetable`.`subject_group_subject_id` = `subject_group_subjects`.`id`inner JOIN subjects on subject_group_subjects.subject_id = subjects.id INNER JOIN staff on staff.id=virtual_class_timetable.staff_id  WHERE `virtual_class_timetable`.`class_id` = " . $class_id . " AND `virtual_class_timetable`.`section_id` = " . $section_id . " AND `virtual_class_timetable`.`day` = " . $this->db->escape($day) . " AND `virtual_class_timetable`.`session_id` = " . $this->current_session . " and staff.is_active=1";
       $query = $this->db->query($sql);

       return $query->result();
   }

   public function addTimeTable($delete_array, $insert_array, $update_array)
   {
        $this->db->trans_start(); # Starting Transaction
        $this->db->trans_strict(false); # See Note 01. If you wish can remove as well
        if (!empty($delete_array)) {

            $this->db->where_in('id', $delete_array);
            $this->db->delete('virtual_class_timetable');

        }

        if (isset($update_array) && !empty($update_array)) {

            $this->db->update_batch('virtual_class_timetable', $update_array, 'id');

        }

        if (isset($insert_array) && !empty($insert_array)) {

            $this->db->insert_batch('virtual_class_timetable', $insert_array);
            $count = count($insert_array);
            $id    = $this->db->insert_id();
            $loop  = $id - $count;
            for ($x = $id; $x > $loop; $x--) {
                $message   = INSERT_RECORD_CONSTANT . " On  subject timetable id " . $x;
                $action    = "Insert";
                $record_id = $x;
                $this->log($message, $record_id, $action);

            }
        }

        $this->db->trans_complete(); # Completing transaction

        if ($this->db->trans_status() === false) {
            $this->db->trans_rollback();
            return false;
        } else {
            $this->db->trans_commit();
            return true;
        }
    }

    public function getBySubjectGroupDayClassSection($subject_group_id, $day, $class_id, $section_id)
    {

        $this->db->select('virtual_class_timetable.*');
        $this->db->from('virtual_class_timetable');
        $this->db->join('subject_group_subjects', 'virtual_class_timetable.subject_group_subject_id = subject_group_subjects.id');
        $this->db->join('staff', 'staff.id = virtual_class_timetable.staff_id');
        $this->db->where('virtual_class_timetable.class_id', $class_id);
        $this->db->where('virtual_class_timetable.section_id', $section_id);
        $this->db->where('virtual_class_timetable.day', $day);
        $this->db->where('virtual_class_timetable.subject_group_id', $subject_group_id);
        $this->db->where('staff.is_active', 1);
        $query = $this->db->get();
        return $query->result();
    }
    
    public function getSubjectByClassandSectionDay($class_id, $section_id, $day)
    {
        $subject_condition = "";
        $userdata          = $this->customlib->getUserData();

        $role_id = $userdata["role_id"];
        if (isset($role_id) && ($userdata["role_id"] == 2) && ($userdata["class_teacher"] == "yes")) {
            if ($userdata["class_teacher"] == 'yes') {

                $my_classes = $this->teacher_model->my_classes($userdata['id']);

                if (!empty($my_classes)) {
                    if (in_array($class_id, $my_classes)) {

                        $subject_condition = "";

                    } else {

                        $my_subjects = $this->teacher_model->get_subjectby_classid($class_id, $section_id, $userdata['id']);
                        
                        $subject_condition = " and subject_group_subjects.id in(" . $my_subjects['subject'] . ")";

                    }
                }else{
                    $my_subjects = $this->teacher_model->get_subjectby_classid($class_id, $section_id, $userdata['id']);

                    $subject_condition = " and subject_group_subjects.id in(" . $my_subjects['subject'] . ")";
                }
            }
        }
        $subject_condition = $subject_condition . " and staff.is_active=1";

        $sql = "SELECT `subject_group_subjects`.`subject_id`,subjects.name as `subject_name`,subjects.code,subjects.type,staff.name,staff.surname,staff.employee_id,`virtual_class_timetable`.* FROM `virtual_class_timetable` JOIN `subject_group_subjects` ON `virtual_class_timetable`.`subject_group_subject_id` = `subject_group_subjects`.`id`inner JOIN subjects on subject_group_subjects.subject_id = subjects.id INNER JOIN staff on staff.id=virtual_class_timetable.staff_id   WHERE `virtual_class_timetable`.`class_id` = " . $class_id . " AND `virtual_class_timetable`.`section_id` = " . $section_id . " AND `virtual_class_timetable`.`day` = " . $this->db->escape($day) . " AND `virtual_class_timetable`.`session_id` = " . $this->current_session . "" . $subject_condition;

        $query = $this->db->query($sql);

        return $query->result();
    }
    
    
    
    //Remove a class id*?
    public function remove($id) {
        
        $this->endmeeting($id);
        $this->db->where('meetingID', $id);
        $this->db->delete('virtual_class_rooms');
        
        
        $query = $this->db->query($sql);
        if ($this->db->affected_rows() > 0) {
                     return true;
          } else {
                    return false;
         }
      
    }


 
  /* //Remove a class id*?
    public function remove($id) {
        print_r($id); die();
        if($this->endmeeting($id)){
            $sql = "DELETE virtual_class_rooms, virtual_class_timetable
            FROM virtual_class_rooms
            LEFT JOIN virtual_class_timetable ON virtual_class_rooms.classId = virtual_class_timetable.class_id 
            WHERE meetingID = '$id'" ;
            $query = $this->db->query($sql);

                if ($this->db->affected_rows() > 0) {
                     return true;
                } else {
                    return false;
                }
        } else {
             return false;
        }
    }
*/


public function class_exists($className, $classId, $sectionId) {


    $data  = array(
        'classId' => $classId,
        'sectionId'=> $sectionId,
    );


    $query = $this->db->where($data)->get('virtual_class_rooms');

    if ($query->num_rows() > 0) {

        return true;

    } else {

        return false;

    }


}

/*Check  virtual classroom Exists*/
public function check_section($classId, $sections)  
    {
        $post_data=$sections;
        extract($post_data); 
        $subjects=array(); 
        foreach($post_data as $key=>$value)  
      {  
         
            $this->db->select('sectionId');
            $this->db->where('sectionId',$value);
            $this->db->where('classId',$classId);
            $this->db->from('virtual_class_rooms');  
            $query=$this->db->get();  
            
      if ($query->num_rows() > 0)  
         return true;
      else  
          return false; 
      }
}

/*Adding a virtual classroom*/
public function addRoom($className, $classId, $sectionArray) {
  $apiResult        = $this->getserver($classId);
  $welcome_note = "%3Cp%3EWelcome%2C+all+students+to+%3Cstrong%3E%25%25CONFNAME%25%25%3C%2Fstrong%3E%21%3C%2Fp%3E%3Cp%3E%3Cstrong%3E%3Cu%3ERules+and+Regulations%3C%2Fu%3E%3C%2Fstrong%3E%3C%2Fp%3E%3Col%3E%3Cli%3EStudents+should+follow+the+instructions+given+for+virtual+class%3C%2Fli%3E%3Cli%3EStudents+should+not+record+video+or+snaps+of+virtual+class%26nbsp%3B%3C%2Fli%3E%3Cli%3EStudents+are+requested+to+use+the+chat+facility+for+any+doubts%3C%2Fli%3E%3C%2Fol%3E%3Cp%3E%3Cbr%3E%3C%2Fp%3E%3Cp%3E%3Cbr%3E%3C%2Fp%3E";
  //$apiquery = 'allowStartStopRecording=true&attendeePW=ap&autoStartRecording=false&logoutURL=https%3A%2F%2Fcvcngr.in%2Fsite%2Fuserlogin&meetingID='.$classId.$sectionId.preg_replace('/\s+/', '', $className).'&moderatorPW=mp&freeJoin=true&name='.$className.'&record=false&welcome='.$welcome_note;
  $apiquery = 'allowStartStopRecording=true&attendeePW=ap&autoStartRecording=false&logoutURL=https%3A%2F%2Fcvcngr.in%2Fsite%2Fuserlogin&meetingID='.$classId.$sectionId.preg_replace('/\s+/', '', $className).'&moderatorPW=mp&freeJoin=true&name='.$className.'&record=false';



  $apichecksum = sha1('create'.$apiquery.$apiResult->authkey);
  $apiurl = $apiResult->url.'/create?'.$apiquery.'&checksum='.$apichecksum;
//  print_r($apiurl); die();
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_URL, $apiurl);
  $response = curl_exec($ch);
  curl_close($ch);
  $xml = simplexml_load_string($response);


  $create_status = $xml->returncode;
  $meeting_ID = $xml->meetingID;
  $attendee_PW = $xml->attendeePW;
  $moderator_PW = $xml->moderatorPW;
  $voice_Bridge = $xml->voiceBridge;
  $message_Key = $xml->messageKey;
  $create_Date = $xml->createDate;
  $create_Time = $xml->createTime;
  $duration = $xml->duration;

  if ($create_status == 'SUCCESS') {
    $room_array = array(
        'meetingName' => $className,
        'classId'=> $classId,
        'meetingID' => $meeting_ID,
        'attendeePW' => $attendee_PW,
        'moderatorPW' => $moderator_PW,
        'voiceBridge' => $voice_Bridge,
        'messageKey' => $message_Key,
        'createDate' => $create_Date,
        'createTime' => $create_Time,
        'duration' => $duration,
    );
    
    $section_group_array = array();
    foreach ($sectionArray as $section_group_key => $section_group_value) {

            $room_array['sectionId'] = $section_group_value;
            

            $section_group_array[] = $room_array;
        }
        
       $ount =  $this->db->insert_batch('virtual_class_rooms', $section_group_array);


    if($ount>0){
        return true;
    }
    else{
        return false;
    }


} elseif ($_GET['type'] == 'member') {
    return false;
}
}


public function getMeetingInfo($meetingId, $classId = NULL, $studentId = NULL) {
    
        $apiResult        = $this->getserver($classId);
      // print_r($apiResult); die();
        $apichecksum = sha1('getMeetingInfo'.'meetingID='.$meetingId.$apiResult->authkey);
        $apiurl = $apiResult->url.'/getMeetingInfo?meetingID='.$meetingId.'&checksum='.$apichecksum;
        
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $apiurl);
        $response = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($response);
    
        $create_status = $xml->returncode;
        $meeting_ID = $xml->messageKey;
        
          
          if ($create_status == 'SUCCESS') {
              foreach ($xml->attendees->attendee as $property) {
                  if((string)$property->fullName==$studentId){
                     return "joined";
                 }
             }
             return true;
          }
          else{
              
              return false;
          }
      }
      
      
public function updateRoom($meedingid, $classId) { 
  $apiResult        = $this->getserver($classId);

  $sql = "SELECT `meetingID`, `meetingName`, `classId`, `sectionId`, `voiceBridge` FROM virtual_class_rooms WHERE `meetingID` =  '$meedingid'";
  $query = $this->db->query($sql);
  $user = $query->row();
  $meetingId = $user->meetingID;
  $meetingName = $user->meetingName;
  $classId = $user->classId;
  $sectionId = $user->sectionId;
  $voiceid = $user->voiceBridge;

  $apiquery = 'allowStartStopRecording=true&attendeePW=ap&autoStartRecording=false&voiceBridge='.$voiceid.'&meetingID='.$meetingId.'&moderatorPW=mp&freeJoin=true&name='.$meetingName.'&record=false';



  $apichecksum = sha1('create'.$apiquery.$apiResult->authkey);
  $apiurl = $apiResult->url.'/create?'.$apiquery.'&checksum='.$apichecksum;


      // die($apiurl);
  $ch = curl_init();
  curl_setopt($ch, CURLOPT_HEADER, 0);
  curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
  curl_setopt($ch, CURLOPT_URL, $apiurl);
  $response = curl_exec($ch);
  curl_close($ch);
  $xml = simplexml_load_string($response);


  $create_status = $xml->returncode;
  $meeting_ID = $xml->meetingID;
  $attendee_PW = $xml->attendeePW;
  $moderator_PW = $xml->moderatorPW;
  $voice_Bridge = $xml->voiceBridge;
  $message_Key = $xml->messageKey;
  $create_Date = $xml->createDate;
  $create_Time = $xml->createTime;
  $duration = $xml->duration;

  if ($create_status == 'SUCCESS')
  {
    return true;
} else{
    return false;
}
}

 public function endmeeting($meetingId) {
             
             $apiResult        = $this->getApi();

       
        $apichecksum = sha1('endmeetingID='.$meetingId.'&password=mp'.$apiResult->authkey);
        $apiurl = $apiResult->url.'/end?meetingID='.$meetingId.'&password=mp&checksum='.$apichecksum;
       
          $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $apiurl);
        $response = curl_exec($ch);
        curl_close($ch);
        $xml = simplexml_load_string($response);
        $create_status = $xml->returncode;
        $meeting_ID = $xml->messageKey;
         
          
          if ($create_status == 'SUCCESS') {
               return true;
              
          }
          else{
              
              return false;
          }
          
      }
      
      
      
       function getMeetingReport() {
            //print_r("Report Not Ready"); die();
             $apiResult        = $this->getApi();
             $apichecksum = sha1('getMeetings'.$apiquery.$apiResult->authkey);
             $url = $apiResult->url.'/getMeetings?'.'&checksum='.$apichecksum;
        //$a= (array)new SimpleXMLElement(file_get_contents($url));
         $ch = curl_init();
        curl_setopt($ch, CURLOPT_HEADER, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_URL, $url);
        $response = curl_exec($ch);
        curl_close($ch);
        $a = (array)new SimpleXMLElement($response);
        if($a["returncode"]=="SUCCESS") {
            $count;
            $teacherscount;
            if($a["meetings"]) {
               foreach($a["meetings"] as $meeting) {
                    $meeting=(array)$meeting;
                    if(!isset($meeting['meetingID'])) { continue; }
                    $data["Meetings"][$meeting["meetingID"]]["participants"]=$meeting["participantCount"];
                    $data["Meetings"][$meeting["meetingID"]]["meetingID"]=$meeting["meetingID"];
                    $data["Meetings"][$meeting["meetingID"]]["Name"]=$meeting["meetingName"];
                    $data["Meetings"][$meeting["meetingID"]]["voice"]=$meeting["voiceBridge"];
                    $data["Meetings"][$meeting["meetingID"]]["running"]=$meeting["running"];
                    $data["Meetings"][$meeting["meetingID"]]["moderators"]=$meeting["moderatorCount"];
                    $data["Meetings"][$meeting["meetingID"]]["totalStudents"]=(int)$meeting["participantCount"]-(int)$meeting["moderatorCount"];
                    $count+=(int)$meeting["participantCount"];
                    $teacherscount+=(int)$meeting["moderatorCount"];
                    $data["zzzzz"] =(int)$meeting["participantCount"];
                    $data["TotalStudents"]+=(int)$meeting["participantCount"];
                    $data["Number"]++;
                    $data["Meetings"][$meeting["meetingID"]]["Video"]=$meeting["videoCount"];
                    $data["Meetings"][$meeting["meetingID"]]["Start"]=date('d.m.Y H:i:s',floor($meeting["startTime"]/1000));
                    if($meeting["attendees"]) foreach($meeting["attendees"] as $att) {
                        $att=(array)$att;
                        if($att["role"]=="MODERATOR") $data["Meetings"][$meeting["meetingID"]]["Liste"]["M"]=array($att["fullName"],($att["hasJoinedVoice"]=="true")? 1:0,($att["hasVideo"]=="true")? 1:0);
                        else $data["Meetings"][$meeting["meetingID"]]["Liste"]["T"][]=array($att["fullName"],($att["hasJoinedVoice"]=="true")? 1:0,($att["hasVideo"]=="true")? 1:0);
                    }
                }
                $data["report"]["total"]= $count;
                $data["report"]["totalStudents"]= $count - $teacherscount;
                $data["report"]["totalTeachers"]= $teacherscount;
                return($data);
            } else return(array("Number"=>0,"Total"=>0));
        } else return(-1);
    }


}