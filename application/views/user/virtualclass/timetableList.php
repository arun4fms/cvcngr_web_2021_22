<div class="content-wrapper" style="min-height: 946px;">
	<section class="content-header">
		<h1>
			<i class="fa fa-calendar-times-o"></i> <?php echo $this->lang->line('class_timetable'); ?> </h1>
		</section>
		<!-- Main content -->
		<section class="content">
			<?php if ($this->session->flashdata('msg')) { ?>
			<?php echo $this->session->flashdata('msg') ?>
			<?php } ?>
			<div class="row">
				<div class="col-md-12">
					<div class="box box-warning">
						<div class="box-header ptbnull">
							<h3 class="box-title titlefix"> <?php echo $this->lang->line('class_timetable'); ?></h3>
							<div class="box-tools pull-right"></div>
						</div>
						<div class="box-body">
							<div class="table-responsive">
								<div class="download_label">
									<?php echo $this->lang->line('class_timetable'); ?></div>
									<?php if (!empty($timetable)) { ?>
									<table class="table table-stripped">
										<thead>
											<tr>
												<?php foreach ($timetable as $tm_key=>$tm_value) { ?>
												<th class="text text-center">
													<?php echo $tm_key; ?>
												</th>
												<?php } ?>
											</tr>
										</thead>
										<tbody>
											<tr>
												<?php foreach ($timetable as $tm_key=>$tm_value) { ?>
												<td class="text text-center">
													<?php if (!$timetable[$tm_key]) { ?>
													<div class="attachment-block clearfix"> <b class="text text-center"><?php echo $this->lang->line('not'); ?> <br><?php echo $this->lang->line('scheduled'); ?></b>
														<br>
													</div>
													<?php } else { foreach ($timetable[$tm_key] as $tm_k=>$tm_kue) { ?>
													 <div class="info-box" style="padding-top: 15px; padding-bottom: 15px;"> <b class="text-green"><?php echo $this->lang->line('subject') ?>: <?php echo $tm_kue->subject_name."  " 	/*. " (" . $tm_kue->code . ")"*/; ?> 

													</b>
													<br> <strong class="text-green"><?php echo $tm_kue->time_from ?></strong>
													<b class="text text-center">-</b>
													<strong class="text-green"><?php echo $tm_kue->time_to; ?></strong>
													<br>
													<form method="post" action="<?php  echo base_url(); ?>user/Virtualclass/studentJoin">
														
														<input type="hidden" id="studentid" name="studentid" value= <?php echo element('studentid',$login_url); ?>>
														<input type="hidden" id="meetingid" name="meetingid" value= <?php echo element('meetingid',$login_url); ?>>
														<input type="hidden" id="loginurl" name="loginurl" value= <?php echo element('login_url',$login_url); ?>>
														
														<button class="btn btn-primary btn-sm" id="submit-buttons" type="submit" >Join Class</button>
													</form>
												</div>
												<?php } } ?>
											</td>
											<?php } ?>
										</tr>
									</tbody>
								</table>
								<?php } ?>
							</div>
							<strong><h4 class="box-title titlefix">Instructions To Use Virtual Class</h4></strong>
							
							<ol>
								<li>Students should not talk unnecessarily because it causes disturbance to online class&nbsp;</li>
								<li>Students can use the public and private chat facility to interact with teachers</li>
								<li>Students are not allowed to turn on web camera due to privacy concern</li>
								<li>Try to use a headset and also a room with low &nbsp;noise disturbance area</li>
								<li>For best performance, it&#39;s recommended to use google chrome browser in your device</li>
								<li>For better network signal try to avoid closed rooms to attend online class</li>
							</ol>
						</div>
					</div>
				</div>
			</div>
		</section>
	</div>